#!/usr/bin/env perl

use warnings;

use strict;

my $debug = 0;

my $wget = "/usr/bin/wget";

my $ical2org = "$ENV{HOME}/.config/scripts/ical2org.pl";

my $base_dir = "$ENV{HOME}/Documents/.calendars";
my $org_dir  = "$ENV{HOME}/Documents/calendars";

my %calendars = (
    'timetable' => 'https://timetable.soton.ac.uk/Feed/Index/jRthV6aULm35GWYft5APY3YsIEwXiqbrEvlWhSkpOS7iSPE3PdV-_6j7sQC3xVkRXI_pRdOqSTinpWu4-s_Xbg2',
    );

chdir $base_dir;
# acad and ooo don't work
my @cals = qw(timetable);

foreach my $cal (@cals) {
    next unless $calendars{$cal};
    my $cmd = "$wget -q -O $cal.ics.new $calendars{$cal} && mv $cal.ics.new $cal.ics && dos2unix $cal.ics && sed '/REFRESH-INTERVAL/d' $cal.ics";
    print STDERR "$cmd\n" if $debug;
    system $cmd;
    next unless -r "$cal.ics";
    $cmd = "$ical2org -c $cal < $base_dir/$cal.ics > $org_dir/$cal.org.new";
    print STDERR "$cmd\n" if $debug;
    system $cmd;

    if ( -s "$org_dir/$cal.org.new" ) {
	$cmd = "cp $org_dir/$cal.org.new $org_dir/$cal.org";
	print STDERR "$cmd\n" if $debug;
	system $cmd;
    }
}
