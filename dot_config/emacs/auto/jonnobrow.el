(TeX-add-style-hook
 "jonnobrow"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "dvipsnames" "10pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "amsthm"))
 :latex)

