(defun dotspacemacs/init ()
    (let ((src (concat dotspacemacs-directory "spacemacs.org"))
           (si (concat dotspacemacs-directory "spacemacs-init.el"))
           (sl (concat dotspacemacs-directory "spacemacs-layers.el"))
           (ui (concat dotspacemacs-directory "user-init.el"))
           (uc (concat dotspacemacs-directory "user-config.el")))
       (when (or (file-newer-than-file-p src si)
                 (file-newer-than-file-p src sl)
                 (file-newer-than-file-p src ui)
                 (file-newer-than-file-p src uc))
         (call-process
          (concat invocation-directory invocation-name)
          nil nil t
          "-q" "--batch" "--eval" "(require 'ob-tangle)"
          "--eval" (format "(org-babel-tangle-file \"%s\")" src)))
       (load-file si))
     )
    (defun dotspacemacs/layers ()
      "Initialization for user code:
    This function is called immediately after `dotspacemacs/init', before layer
    configuration.
    It is mostly for variables that should be set before packages are loaded.
    If you are unsure, try setting them in `dotspacemacs/user-config' first."
        (let ((sl (concat dotspacemacs-directory "spacemacs-layers.el")))
        (load-file sl))
     )
    (defun dotspacemacs/user-init ()
      "Initialization for user code:
    This function is called immediately after `dotspacemacs/init', before layer
    configuration.
    It is mostly for variables that should be set before packages are loaded.
    If you are unsure, try setting them in `dotspacemacs/user-config' first."
        (let ((ui (concat dotspacemacs-directory "user-init.el")))
        (load-file ui))
     )
     (defun dotspacemacs/user-config ()
       "Configuration for user code:
     This function is called at the very end of Spacemacs startup, after layer
     configuration.
     Put your configuration code here, except for variables that should be set
     before packages are loaded."
       (let ((uc (concat dotspacemacs-directory "user-config.el")))
         (load-file uc))
     )
