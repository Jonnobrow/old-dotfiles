\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{jonnobrow}[2019/12/09 Jonnobrow Class]

\LoadClass[a4paper, dvipsnames, 10pt]{article}

\RequirePackage{amsthm}
\newtheorem*{theorem}{Theorem}
\newtheorem*{corollary}{Corollary}
\newtheorem*{lemma}{Lemma}
\newtheorem*{definition}{Definition}