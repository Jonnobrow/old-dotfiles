#+title: Jonnobrow's Dotfiles
#+email: contact AT jonnobrow DOT me
#+author: Jonathan Bartlett

* Preface
This is the init.el file 
** Spacemacs Init Function
   Executed first, must be present as it is responsible for loading the rest of ~spacemacs.org~
 #+begin_src emacs-lisp :exports none
   ;; -*- mode: emacs-lisp -*-
   ;; This file is loaded by Spacemacs at startup.
   ;; It must be stored in your home directory.
   (defun dotspacemacs/init ()
    (let ((src (concat dotspacemacs-directory "spacemacs.org"))
           (si (concat dotspacemacs-directory "spacemacs-init.el"))
           (sl (concat dotspacemacs-directory "spacemacs-layers.el"))
           (ui (concat dotspacemacs-directory "user-init.el"))
           (uc (concat dotspacemacs-directory "user-config.el")))
       (when (or (file-newer-than-file-p src si)
                 (file-newer-than-file-p src sl)
                 (file-newer-than-file-p src ui)
                 (file-newer-than-file-p src uc))
         (call-process
          (concat invocation-directory invocation-name)
          nil nil t
          "-q" "--batch" "--eval" "(require 'ob-tangle)"
          "--eval" (format "(org-babel-tangle-file \"%s\")" src)))
       (load-file si))
     )
 #+end_src
** Spacemacs Layers Config
   All this does is load the layers config
  #+begin_src emacs-lisp :exports none
    (defun dotspacemacs/layers ()
      "Initialization for user code:
    This function is called immediately after `dotspacemacs/init', before layer
    configuration.
    It is mostly for variables that should be set before packages are loaded.
    If you are unsure, try setting them in `dotspacemacs/user-config' first."
        (let ((sl (concat dotspacemacs-directory "spacemacs-layers.el")))
        (load-file sl))
     )
  #+end_src
** User Init
  All this does is load the user init function 
  #+begin_src emacs-lisp :exports none
    (defun dotspacemacs/user-init ()
      "Initialization for user code:
    This function is called immediately after `dotspacemacs/init', before layer
    configuration.
    It is mostly for variables that should be set before packages are loaded.
    If you are unsure, try setting them in `dotspacemacs/user-config' first."
        (let ((ui (concat dotspacemacs-directory "user-init.el")))
        (load-file ui))
     )
  #+end_src
** User Config
   Loads the user config from file
   #+begin_src emacs-lisp :exports none
     (defun dotspacemacs/user-config ()
       "Configuration for user code:
     This function is called at the very end of Spacemacs startup, after layer
     configuration.
     Put your configuration code here, except for variables that should be set
     before packages are loaded."
       (let ((uc (concat dotspacemacs-directory "user-config.el")))
         (load-file uc))
     )
   #+end_src
* dotspacemacs/init
*Initialization*: This function is called at the very beginning of Spacemacs startup before layer configuration.
It should only modify the values of Spacemacs settings.
#+begin_src emacs-lisp :tangle spacemacs-init.el
 (setq-default
#+end_src
** Dumper
#+BEGIN_SRC emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-enable-emacs-pdumper nil
  dotspacemacs-emacs-pdumper-executable-file "emacs"
  dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"
#+end_src
** ELPA
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-elpa-https t
  dotspacemacs-elpa-timeout 5
  dotspacemacs-gc-cons '(100000000 0.1)
  dotspacemacs-use-spacelpa t
  dotspacemacs-verify-spacelpa-archives t
  dotspacemacs-elpa-subdirectory 'emacs-version
#+end_src
** Check for Update
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-check-for-update nil
#+end_src
** Editing Style
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-editing-style 'vim
#+end_src
** Startup Settings
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-startup-banner '100
  dotspacemacs-startup-lists '((agenda . 5))
  dotspacemacs-startup-buffer-responsive t
  dotspacemacs-loading-progress-bar t
  dotspacemacs-fullscreen-at-startup nil
  dotspacemacs-fullscreen-use-non-native nil
  dotspacemacs-maximized-at-startup nil
  dotspacemacs-undecorated-at-startup nil
#+end_src
** New Buffer Settings
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-new-empty-buffer-major-mode 'text-mode
  dotspacemacs-scratch-mode 'text-mode
  dotspacemacs-initial-scratch-message nil
#+end_src
** Theming
Set the theme.
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-themes '(gruvbox)
#+end_src

Set the theme for the Spaceline. 
Supported themes are:
- ~spacemacs~
- ~all-the-icons~
- ~custom~
- ~doom~
- ~vim-powerline~
- ~vanilla~
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)
#+end_src

Set the cursor color.
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-colorize-cursor-according-to-state t
#+end_src

Set the font.
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-default-font '("Source Code Pro"
                              :size 15
                              :weight normal
                              :width normal)
#+end_src

Unicode symbols in mode line.
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-mode-line-unicode-symbols t
#+end_src

Transparency.
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-active-transparency 90
  dotspacemacs-inactive-transparency 90
#+end_src 

Title Format.
#+begin_src emacs-lisp :tangle spacemacs-init.el
  ;; Format specification for setting the frame title.
  ;; %a - the `abbreviated-file-name', or `buffer-name'
  ;; %t - `projectile-project-name'
  ;; %I - `invocation-name'
  ;; %S - `system-name'
  ;; %U - contents of $USER
  ;; %b - buffer name
  ;; %f - visited file name
  ;; %F - frame name
  ;; %s - process status
  ;; %p - percent of buffer above top of window, or Top, Bot or All
  ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
  ;; %m - mode name
  ;; %n - Narrow if appropriate
  ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
  ;; %Z - like %z, but including the end-of-line format
  ;; (default "%I@%S")
  dotspacemacs-frame-title-format "%I@%S"

  ;; Format specification for setting the icon title format
  ;; (default nil - same as frame-title-format)
  dotspacemacs-icon-title-format nil
#+end_src
** Key Bindings
#+begin_src emacs-lisp :tangle spacemacs-init.el
  ;; The leader key (default "SPC")
  dotspacemacs-leader-key "SPC"
  ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
  ;; (default "SPC")
  dotspacemacs-emacs-command-key "SPC"
  ;; The key used for Vim Ex commands (default ":")
  dotspacemacs-ex-command-key ":"
  ;; The leader key accessible in `emacs state' and `insert state'
  ;; (default "M-m")
  dotspacemacs-emacs-leader-key "M-m"
  ;; Major mode leader key is a shortcut key which is the equivalent of
  ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
  dotspacemacs-major-mode-leader-key ","
  ;; Major mode leader key accessible in `emacs state' and `insert state'.
  ;; (default "C-M-m")
  dotspacemacs-major-mode-emacs-leader-key "C-M-m"
  ;; These variables control whether separate commands are bound in the GUI to
  ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
  ;; Setting it to a non-nil value, allows for separate commands under `C-i'
  ;; and TAB or `C-m' and `RET'.
  ;; In the terminal, these pairs are generally indistinguishable, so this only
  ;; works in the GUI. (default nil)
  dotspacemacs-distinguish-gui-tab nil
#+end_src
** Layouts
#+begin_src emacs-lisp :tangle spacemacs-init.el
  ;; Name of the default layout (default "Default")
  dotspacemacs-default-layout-name "Default"
  ;; If non-nil the default layout name is displayed in the mode-line.
  ;; (default nil)
  dotspacemacs-display-default-layout nil
  ;; If non-nil then the last auto saved layouts are resumed automatically upon
  ;; start. (default nil)
  dotspacemacs-auto-resume-layouts nil
  ;; If non-nil, auto-generate layout name when creating new layouts. Only has
  ;; effect when using the "jump to layout by number" commands. (default nil)
  dotspacemacs-auto-generate-layout-names nil
#+end_src
** Large File Size
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-large-file-size 50
#+end_src
** Autosave Settings
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-auto-save-file-location 'original
  dotspacemacs-max-rollback-slots 5
#+end_src
** Paste Transient State
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-enable-paste-transient-state nil
#+end_src
** Which Key Settings
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-which-key-delay 0.4
  dotspacemacs-which-key-position 'bottom
#+end_src
** Usability
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-smooth-scrolling t
  dotspacemacs-line-numbers nil
  dotspacemacs-folding-method 'evil
  dotspacemacs-smartparens-strict-mode nil
  dotspacemacs-smart-closing-parenthesis nil
  dotspacemacs-highlight-delimiters 'all
  dotspacemacs-whitespace-cleanup nil
#+end_src 
** Transient State
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-show-transient-state-title t
  dotspacemacs-show-transient-state-color-guide t
#+end_src 
** Server
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-enable-server nil
  dotspacemacs-server-socket-dir nil
  dotspacemacs-persistent-server nil
#+end_src 
** Other
#+begin_src emacs-lisp :tangle spacemacs-init.el
  dotspacemacs-switch-to-buffer-prefers-purpose nil
  dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")
  dotspacemacs-zone-out-when-idle nil
  dotspacemacs-pretty-docs nil
   #+END_SRC
** Switch to home file
#+begin_src emacs-lisp :tangle yes
(find-file "~/Documents/home.org")
#+end_src
** Trailing Bracket
#+begin_src emacs-lisp :tangle spacemacs-init.el
)
#+end_src 
* dotspacemacs/layers
*Layer Configuration*: This function should only modify configuration layer settings.
#+BEGIN_SRC emacs-lisp :tangle spacemacs-layers.el
(setq-default
#+end_src
** Spacemacs Distribution
#+begin_src emacs-lisp :tangle spacemacs-layers.el
  dotspacemacs-distribution 'spacemacs
#+end_src
** Lazy Installation
#+begin_src emacs-lisp :tangle spacemacs-layers.el
  dotspacemacs-enable-lazy-installation 'unused
  dotspacemacs-ask-for-lazy-installation t
#+end_src
** Layer Config
*** Layer Path
#+begin_src emacs-lisp :tangle spacemacs-layers.el
  dotspacemacs-configuration-layer-path '()
#+end_src
*** Configuration Layers
   #+begin_src emacs-lisp :tangle spacemacs-layers.el
 ;; List of configuration layers to load.
  dotspacemacs-configuration-layers
  '(
   #+end_src 
**** Programming Languages
 #+begin_src emacs-lisp :tangle spacemacs-layers.el
    lsp
    (haskell :variables
             haskell-completion-backend 'lsp)
    emacs-lisp
    python
    html
    (java :variables
          java-completion-backend 'lsp-java)
    (c-c++ :variables
           c-c++-backend 'lsp-ccls
           c-c++-enable-google-style t)
    semantic
    cscope
    csharp
 #+end_src
**** Markup Languages
    #+begin_src emacs-lisp :tangle spacemacs-layers.el
    org
    markdown
    latex
    yaml
    systemd
    #+end_src 
**** Editor
    #+begin_src emacs-lisp :tangle spacemacs-layers.el
    spell-checking
    syntax-checking
    treemacs
    ivy
    auto-completion
    better-defaults
    git
    shell
    #+end_src 
**** ElFeed
#+begin_src emacs-lisp :tangle spacemacs-layers.el
(elfeed :variables rmh-elfeed-org-files (list "~/Documents/feeds.org"))
#+end_src
**** Trailing Bracket
    #+begin_src emacs-lisp :tangle spacemacs-layers.el
    )
    #+end_src 
** Additional, Frozen and Excluded Packages
#+begin_src emacs-lisp :tangle spacemacs-layers.el
  dotspacemacs-additional-packages '(openwith
                                     org-ref)
  dotspacemacs-frozen-packages '()
  dotspacemacs-excluded-packages '()
  dotspacemacs-install-packages 'used-only
#+END_SRC
** Trailing Bracket
#+begin_src emacs-lisp :tangle spacemacs-layers.el
)
#+end_src
* dotspacemacs/user-init
#+BEGIN_SRC emacs-lisp :tangle user-init.el
#+END_SRC
* dotspacemacs/user-config
Configuration function for user code.

This function is called at the very end of Spacemacs initialization after layers configuration.
This is the place where most of your configurations should be done.
Unless it is explicitly specified that a variable should be set before a package is loaded,
you should place your code here.

** Org Mode Configuration
*** Org Export
**** Org LaTex Export
***** Imports
#+begin_src emacs-lisp :tangle user-config.el
 (require 'ox)
 (require 'cl)
#+end_src
***** Compiler
     #+begin_src emacs-lisp :tangle user-config.el
 (setq org-latex-compiler "pdflatex")
(setq org-latex-pdf-process (list
                             "pdflatex %f"
                             "pdflatex %f"
                             "pdflatex %f"))
     #+end_src 
***** Listings
     #+begin_src emacs-lisp :tangle user-config.el
(setq org-src-preserve-indentation t)
(setq org-latex-listings t)
(setq org-latex-listings-options '(("numbers" "left")
                                   ("backgroundcolor" "\\color{white}")
                                   ("commentstyle" "\\color{green}")
                                   ("keywordstyle" "\\color{magenta}")
                                   ("numberstyle" "\\tiny\\color{gray}")
                                   ("stringstyle" "\\color{purple}")
                                   ("basicstyle" "\\ttfamily\\scriptsize")
                                   ("breaklines" "true")
                                   ("showtabs" "false")
                                   ("showspaces" "false")
                                   ("showstringspaces" "false")
                                   ("keepspaces" "true")))
     #+end_src 
***** Classes
     #+begin_src emacs-lisp :tangle user-config.el
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))
(setq org-latex-default-class "article")
(add-to-list 'org-latex-classes
  '("article"
    "\\documentclass[10pt,a4paper]{article}\n[DEFAULT-PACKAGES]
\n[PACKAGES]\n[EXTRA]\n\\setlist{noitemsep}"
    ("\\section{%s}" . "\\section*{%s}")
    ("\\subsection{%s}" . "\\subsection*{%s}")
    ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
    ("\\paragraph{%s}" . "\\paragraph*{%s}")
    ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
     #+end_src 
***** Default Packages
     #+begin_src emacs-lisp :tangle user-config.el
(add-to-list 'org-latex-packages-alist '("" "mathtools"))
(add-to-list 'org-latex-packages-alist '("" "fullpage"))
(add-to-list 'org-latex-packages-alist '("" "amsthm"))
(add-to-list 'org-latex-packages-alist '("" "float"))
(add-to-list 'org-latex-packages-alist '("" "listings"))
(add-to-list 'org-latex-packages-alist '("" "xcolor"))
(add-to-list 'org-latex-packages-alist '("" "textcomp"))
(add-to-list 'org-latex-packages-alist '("" "enumitem"))
     #+end_src 
***** Other
      #+begin_src emacs-lisp :tangle user-config.el
(setq org-export-with-smart-quotes t)
(setq org-export-headline-levels 5)
      #+end_src 
*** Org Publish
#+begin_src emacs-lisp :tangle user-config.el
  (setq org-publish-project-alist '(
#+end_src        
**** Notes
#+begin_src emacs-lisp :tangle user-config.el
          ("notes"
           :base-directory "~/Documents/uni/"
           :base-extension "org"
           :publishing-directory "~/Documents/uni/notes-pdf/"
           :recursive t
           :publishing-function publish-function-notes-pdf
           :headline-levels 5)
#+end_src
**** Publish alist Trailing Brackets
#+BEGIN_SRC emacs-lisp :tangle user-config.el
 )) 
#+END_SRC

**** Supporting Functions
#+begin_src emacs-lisp :tangle user-config.el
(defun publish-function-notes-pdf (PLIST FILENAME PUB-DIR)
    (if (or (equal (file-name-nondirectory FILENAME) "notes.org")
            (equal (file-name-nondirectory FILENAME) "theindex.org"))
        (org-latex-publish-to-pdf PLIST FILENAME PUB-DIR)))

   (setq org-html-htmlize-output-type 'css)
#+end_src 
*** Org TODO
**** Set All TODO entries to DONE when subentries DONE  
 #+begin_src emacs-lisp :tangle user-config.el
    (defun org-summary-todo (n-done n-not-done)
      "Switch entry to DONE when all subentries are done, to TODO otherwise."
      (let (org-log-done org-log-states)   ; turn off logging
        (org-todo (if (= n-not-done 0) "DONE" "TODO"))))
    (add-hook 'org-after-todo-statistics-hook 'org-summary-todo)
 #+end_src
**** Keywords
#+begin_src emacs-lisp :tangle user-config.el
  (setq org-todo-keywords
        '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d!)" "CANCELED(c@/!)")))
#+end_src 
*** Org Agenda
#+begin_src emacs-lisp :tangle user-config.el
(setq org-agenda-skip-scheduled-if-deadline-is-shown t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-deadline-if-done t)

(setq org-agenda-files (list "~/Documents/tickler.org"
                             "~/Documents/university.org"
                             "~/Documents/home.org"
                             "~/Documents/journal.org"
                             "~/Documents/inbox.org"
                             "~/Documents/calendars/"
                             "~/Documents/uni/COMP2212/assignments/comp2212-cw/docs/todo.org"))
#+end_src 
*** Org Capture
#+begin_src emacs-lisp :tangle user-config.el
(setq org-capture-templates `(
                              ("t" "Inbox Entry"
                               entry (file+headline "~/Documents/inbox.org" "INBOX")
                               "* TODO %i%?\n\nFrom: %a")
                              ("T" "Tickler Entry"
                               entry (file "~/Documents/tickler.org")
                               "* %i%? \n %U")
                              ("n" "Note"
                               entry (file+olp+datetree "~/Documents/notes.org")
                               "* Note %T %?"
                               :empty-lines 1)
                              ("j" "Journal Entry"
                               entry (file "~/Documents/journal.org")
                               "* Journal %t\n\n%q"
                               :empty-lines 1)
                              ("l" "Logbook Entry"
                               entry (file+olp+datetree "~/Documents/uni/logbook.org")
                               "* Logbook Entry %T %^g\n%?"
                               :empty-lines 0)
))

(setq org-refile-targets '(("~/Documents/tickler.org" :maxlevel . 2)
                           ("~/Documents/home.org" :maxlevel . 2)
                           ("~/Documents/notes.org" :maxlevel . 3)
                           ("~/Documents/university.org" :maxlevel . 2)))

(setq org-archive-location "~/Documents/archive/archive.org::* Archived from %s")

(require 'ido)
(defun get-notes-file-for-module ()
  "Returns the notes file path for the module"
  (interactive)
  (let* (
        (module (ido-completing-read "Module: " (directory-files "~/Documents/uni/"))))
  (set-buffer (org-capture-target-buffer (expand-file-name (concat "~/Documents/uni/" module "/notes.org"))))
  (goto-char (point-max))))

(setq org-agenda-custom-commands
      '(("i" tags "+@inbox")))
#+end_src
*** Org Tags
#+begin_src emacs-lisp :tangle user-config.el
#+end_src 
*** Indentation and Truncation
#+begin_src emacs-lisp :tangle user-config.el
   (setq-default org-startup-truncated nil)
   (setq org-adapt-indentation nil)
#+end_src
*** Org Babel
#+begin_src emacs-lisp :tangle user-config.el
   (org-babel-do-load-languages
    'org-babel-load-languages
    '((haskell . t)
      (shell . t)
      (python . t)
      (gnuplot . t)
      (R . t)
      (C . t)))
#+end_src
*** Org Ref
#+begin_src emacs-lisp :tangle user-config.el
   (setq org-ref-bibliography-notes "~/Documents/biblio/notes.org"
         org-ref-default-bibliography '("~/Documents/biblio/references.bib")
         org-ref-pdf-directory "~/Documents/biblio/bibtex-pdfs/")
   (setq org-ref-completion-library 'org-ref-ivy-cite)
#+end_src 
*** Org Link Handling
**** Images Loaded from Central Directory
#+begin_src emacs-lisp :tangle user-config.el
;; Directory for images
(defvar img-d "/home/jonnobrow/Pictures/OrgMode/")

(org-link-set-parameters "img"
                         :complete 'org-img-link-complete
                         :follow 'org-img-link-follow
                         :export 'org-img-link-export)

(defun org-img-link-follow (link)
  "Visit the img with imv."
  (start-process "view-image" nil "imv" (expand-file-name link img-d)))

(defun org-img-link-complete ()
  "Create an org-link target string to a package documentation."
  (concat "img:" (file-relative-name (read-file-name "File: " img-d img-d) img-d)))

;; Warning: The below requires \graphicspath{img-d}

(defun org-img-link-export (link desc format)
  (cond
            ((eq 'html format)
             (format "<img src=\"%s\"/>" (expand-file-name link img-d)))
            ((eq 'latex format)
             (format "\\begin{figure}[H]\\centering\\includegraphics[width=0.9\\textwidth]{%s}\\caption{%s}\\end{figure}" (expand-file-name link img-d) desc))))

#+end_src
** Writeroom
#+begin_src emacs-lisp :tangle user-config.el
   (setq writeroom-width 120)
   (add-hook 'org-mode-hook 'writeroom-mode)
   (setq writeroom-maximize-window nil)
#+end_src 
** Open Links
#+begin_src emacs-lisp :tangle user-config.el
(setq openwith-associations '(("\\.pdf\\'" "zathura" (file))
                              ("\\.mp4\\'" "mpv" (file))))
(openwith-mode 1)
#+end_src 

[[https://emacs.stackexchange.com/a/52920][Credit]]
#+begin_src emacs-lisp :tangle user-config.el
(org-add-link-type "pdf" 'org-pdf-open nil)

(defun org-pdf-open (link)
  "Where page number is 105, the link should look like:
   [[pdf:/path/to/file.pdf#page=105][My description.]]"
  (let* ((path+page (split-string link "#page="))
         (pdf-file (car path+page))
         (page (car (cdr path+page))))
    (start-process "view-pdf" nil "zathura" "-P" page pdf-file)))
#+end_src
** ispell
#+begin_src emacs-lisp :tangle user-config.el
   (setq-default ispell-dictionary "en_GB")
#+end_src
** General Truncation and Word Wrap
#+begin_src emacs-lisp :tangle user-config.el
   (setq-default truncate-lines nil)
   (setq-default word-wrap t)
#+end_src 
** LaTeX
*** Reftex
#+begin_src emacs-lisp :tangle user-config.el
  (setq reftex-default-bibliography '("~/Documents/biblio/references.bib"))
#+end_src   
*** TeX-source-correlate
#+begin_src emacs-lisp :tangle user-config.el
  (setq TeX-source-correlate t)
  (setq TeX-global-PDF-mode t)
#+end_src 
** Git
#+begin_src emacs-lisp :tangle user-config.el
  (global-git-commit-mode t)
#+END_SRC
** Chezmoi Hook
#+begin_src emacs-lisp :tangle user-config.el
(defun dotspacemacsfile_hook ()
 (when (string= (file-name-nondirectory (buffer-file-name)) "spacemacs.org")
     (shell-command "/bin/chezmoi add /home/jonnobrow/.config/emacs/spacemacs.org")
     (message "Chezmoi added spacemacs")
     )
 )
 (add-hook 'after-save-hook 'dotspacemacsfile_hook)
#+end_src
** Auto Save
#+begin_src emacs-lisp :tangle user-config.el
(setq auto-save-interval 50)
(setq auto-save-timeout 10)
#+end_src
** Load Path
#+begin_src emacs-lisp :tangle user-config.el
(add-to-list 'load-path "~/Documents/new_website/ox-thtml.el")
#+end_src
** LSP Java
#+begin_src emacs-lisp :tangle user-config.el
(setq lsp-java-format-settings-url "https://raw.githubusercontent.com/google/styleguide/gh-pages/eclipse-java-google-style.xml")
(setq lsp-java-format-settings-profile "GoogleStyle")
(add-hook 'java-mode-hook (defun my-set-java-tab-width () (setq c-basic-offset 4)))
#+end_src
