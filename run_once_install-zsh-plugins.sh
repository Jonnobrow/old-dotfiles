#!/bin/sh
mkdir ~/.local/share/zsh -p
mkdir ~/.cache/zsh -p
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ~/.local/share/zsh/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-history-substring-search.git ~/.local/share/zsh/zsh-history-substring-search
git clone https://github.com/zsh-users/zsh-autosuggestions.git ~/.local/share/zsh/zsh-autosuggestions
